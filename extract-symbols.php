<?php

use PhpParser\Node;
use PhpParser\ParserFactory;

require_once __DIR__ . '/vendor/autoload.php';

function get_parser() {
	static $parser;

	if ( empty( $parser ) ) {
		$parser = ( new ParserFactory )->create( ParserFactory::PREFER_PHP7 );
	}

	return $parser;
}

function resolve( Node $node ) {
	if ( $node instanceof Node\Stmt\Namespace_ ) {
		$namespace = join( '\\', $node->name->parts );
		$symbols   = array();

		foreach ( $node->stmts as $subnode ) {
			foreach ( resolve( $subnode ) as $result ) {
				$symbols[] = $namespace . '\\' . $result;
			}
		}

		return $symbols;
	} elseif ( $node instanceof Node\Stmt\Class_ ) {
		return array( $node->name->name );
	} elseif ( $node instanceof Node\Stmt\Function_ ) {
		return array( $node->name->name );
	} elseif ( $node instanceof Node\Stmt\If_ ) {
		$symbols = array();

		foreach ( $node->stmts as $subnode ) {
			foreach ( resolve( $subnode ) as $result ) {
				$symbols[] = $result;
			}
		}

		return $symbols;
	} elseif ( $node instanceof Node\Stmt\Trait_ ) {
		return array( $node->name->name );
	} elseif ( $node instanceof Node\Stmt\Interface_ ) {
		return array( $node->name->name );
	} elseif (
		$node instanceof Node\Stmt\Expression
		&& $node->expr instanceof Node\Expr\FuncCall
		&& in_array( 'define', $node->expr->name->parts )
	) {
		return array( $node->expr->args[0]->value->value );
	} else {
		//var_dump( $node );
	}

	return array();
}

function get_files( string $folder ) {
	$found = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $folder ), RecursiveIteratorIterator::SELF_FIRST );
	$files = array();

	foreach ( $found as $file ) {
		if ( preg_match( "/\/vendor\//i", $file ) ) {
			continue;
		}

		if ( preg_match( "/\/wp-content\//i", $file ) ) {
			continue;
		}

		if ( preg_match( "/\.php$/i", $file ) ) {
			$files[] = $file;
		}
	}

	return $files;
}

function extract_symbols( string $where, string $result ) {
	$files   = get_files( $where );
	$symbols = array();

	foreach ( $files as $file ) {
		try {
			$ast = get_parser()->parse( file_get_contents( $file ) );

			foreach ( $ast as $node ) {
				$symbols = array_merge( $symbols, resolve( $node ) );
			}
		} catch ( Error $error ) {
			echo "Parse error: {$error->getMessage()}\n";

			return;
		}
	}

	$symbols = array_unique( $symbols );

	$content = join( array(
		"<?php return array('",
		join( '\',\'', array_map( 'addslashes', $symbols ) ),
		"');",
	) );

	file_put_contents( $result, $content );

	echo ">>> " . count( $symbols ) . " symbols exported to " . $result . "\n";
}

//extract_symbols( __DIR__ . '/tests', 'symbols/test.php' );
extract_symbols( __DIR__ . '/wordpress', 'symbols/wordpress.php' );
extract_symbols( __DIR__ . '/plugins/woocommerce', 'symbols/woocommerce.php' );
