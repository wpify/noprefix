# Symbols from WordPress and WooCommerce to use with php-scoper

Extracts symbols (functions, constants, classes, interfaces, traits) from the WordPress and WooComerce to filter that symbols out in php-scoper.

## Usage

Use the symbols in `scoper.inc.php` in the patchers. Example `scoper.inc.php`:

```php
<?php declare( strict_types=1 );

use Isolated\Symfony\Component\Finder\Finder;

$prefix = 'WpifyWooDeps';

$whitelist_wordpress   = require_once __DIR__ . '/vendor/wpify/noprefix/symbols/wordpress.php';
$whitelist_woocommerce = require_once __DIR__ . '/vendor/wpify/noprefix/symbols/woocommerce.php';
$whitelist             = array_merge_recursive( $whitelist_wordpress, $whitelist_woocommerce );

return array(
	'prefix'                     => $prefix,
	'finders'                    => array(
		Finder::create()
		      ->files()
		      ->ignoreVCS( true )
		      ->notName( '/LICENSE|.*\\.md|.*\\.dist|Makefile|composer\\.json|composer\\.lock/' )
		      ->exclude( [
			      'doc',
			      'test',
			      'test_old',
			      'tests',
			      'Tests',
			      'vendor-bin',
			      'node_modules',
			      'scoper.inc.php',
		      ] )
		      ->in( __DIR__ . '/deps/unprefixed' ),
		Finder::create()
		      ->append( array(
			      __DIR__ . '/deps/unprefixed/composer.json',
			      __DIR__ . '/deps/unprefixed/composer.lock'
		      ) ),
	),
	'patchers'                   => array(
		function ( string $filePath, string $prefix, string $content ) use ( $whitelist ): string {
			if ( strpos( $filePath, 'guzzlehttp/guzzle/src/Handler/CurlFactory.php' ) !== false ) {
				$content = str_replace( 'stream_for($sink)', 'Utils::streamFor()', $content );
			}

			usort( $whitelist, function ( $a, $b ) {
				return strlen( $b ) - strlen( $a );
			} );

			$count        = 0;
			$searches     = array();
			$replacements = array();

			foreach ( $whitelist as $symbol ) {
				$searches[]     = "\\$prefix\\$symbol";
				$replacements[] = "\\$symbol";

				$searches[]     = "use $prefix\\$symbol";
				$replacements[] = "use $symbol";
			}

			$content = str_replace( $searches, $replacements, $content, $count );

			return $content;
		},
	),
	'whitelist'                  => array(),
	'whitelist-global-constants' => false,
	'whitelist-global-classes'   => false,
	'whitelist-global-functions' => false,
);
```